#!/usr/bin/env python
from utils import Utils

BASE_URL = 'https://thematter.co'


class TheMatter:

    @classmethod
    def getCategoryLinks(self,content):
        links = []
        for i in content.find_all("a"):
            if (i.parent.name == 'li' ):
                if ('menu-item-object-category' in i.parent.get('class')):
                    links.append(Utils.convertURLEncodingToUnicode(i.get('href')))
                    #def convertIRIToURL(self,iri):
        return links


    @classmethod
    def getBaseContent(self):
        global BASE_URL
        return Utils.getContent(BASE_URL)


    
    @classmethod
    def getArticles(self,content):
        articles = []
        for i in content.find_all("a"):
            try:
                a = int(i.get('href')[-3:-1])
                articles.append(i.get('href'))
            except:
                a='a'
        return articles

    @classmethod
    @Utils.exceptionHandler
    def getTitleFromArticle(self, content):
        return str(content.find("h1").string)

    @classmethod
    @Utils.exceptionHandler
    def getDateFromArticle(self,content):
        date = content.find("i", attrs={"class": "fa-clock-o"}).next_sibling
        return date[date.find("Posted On ")+len("Posted On"):].strip()

    @classmethod
    @Utils.exceptionHandler
    def getBodyFromArticle(self,content):
        body = ''
        for text in content.find("div",attrs={"class": "post_content_wrapper"}).children:
            if (text.name != "div"):
                if (text.string != None):
                    body = body + text.string.strip()
        return body


    @classmethod
    def getArticleAttrs(self,content):
        title = TheMatter.getTitleFromArticle(content)
        #print(title)
        date = TheMatter.getDateFromArticle(content)
        #print(date)
        body = TheMatter.getBodyFromArticle(content)
        #print(body)
        dic = {}
        dic['title'] = title
        dic['date'] = date
        dic['body'] = body
        return dic


    @classmethod
    def getArticle(self):
        url = 'https://thematter.co'
        #links = getTheMatterLinks(getContent(url))
        #slink = 'https://thematter.co/category/science-tech/health'
        #articles = getTheMatterArticles(getContent(slink)):
        sarticle = 'https://thematter.co/quick-bite/abortion-law-in-thai/122466'
        articledic = TheMatter.getArticleAttrs(Utils.getContent(sarticle))
        articledic = TheMatter.getArticleAttrs(Utils.getContent("https://www.google.com"))
        return articledic



if __name__ == '__main__':
    TheMatter.getArticle()

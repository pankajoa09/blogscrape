#!/usr/bin/env python
from bs4 import BeautifulSoup
from urllib.request import Request,urlopen
from urllib.parse import quote,unquote,urlsplit,urlunsplit
import unicodedata
import re
import os
import json
from urllib.error import HTTPError






class Utils:

    @classmethod
    def convertIRIToURL(self,iri):
        split_url = list(urlsplit(iri))
        split_url[2] = quote(split_url[2])    # the third component is the path of the URL/IRI
        url = urlunsplit(split_url)
        return url

    @classmethod
    def convertURLEncodingToUnicode(self,url):
        return unquote(url)


    @classmethod
    def exceptionHandler(self,func):
        def inner_function(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except AttributeError:
                print('attribute error, Nonetype')
        return inner_function

    @classmethod
    def getContent(self,url):
        try:
            req = Request(url)
            response = urlopen(req)
            page = response.read()
            content = BeautifulSoup(page, features="html5lib")
            return content
        except UnicodeEncodeError:
            print('unicode error: ',url)
            return Utils.getContent(Utils.convertIRIToURL(url))
        except HTTPError as err:
            print(url)
            print('u34',end='')
            print(err.code)
            if err.code == 403:
                content = Utils.getContentHard(url)
                return content
            if err.code == 404:
                content= BeautifulSoup("", features="html5lib")
                return content
            else:
                raise


    @classmethod
    def getContentHard(self,url):
        try:
            req = Request(url)
            req.add_header('Accept','text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9')
            req.add_header('Referer', 'https://www.google.com')
            req.add_header("User-Agent","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36")
            response = urlopen(req)
            page = response.read()
            content = BeautifulSoup(page, features="html5lib")
            return content
        except HTTPError as err:
            print('u60',end='')
            if err.code == 403:
                print("Can't get in and out of ideas.")
                content = BeautifulSoup("", features="html5lib")
            if err.code == 404:
                print("404 page not found")
                content = BeautifulSoup("", features="html5lib")
            else:
                raise
        return content

#!/usr/bin/env python
from utils import Utils

BASE_URL = 'https://www.thairath.co.th'

class Thairath:

    @classmethod
    def getContent(self,url):
        return Utils.getContent(url)

    @classmethod
    def getCategoryLinks(self,content):
        global BASE_URL
        links = []
        for i in content.find_all("a"):
            if (i.parent.name == 'li'):
                if (i.get('href')[0] == '/'):
                    links.append(BASE_URL + i.get('href'))
        return links

    @classmethod
    def getArticles(self,content):
        global BASE_URL
        articles = []
        for i in content.find_all("a"):
            try:
                a = int(i.get('href')[-3:-1])
                if (i.get('href')[0] == '/'):
                    articles.append(BASE_URL + i.get('href'))
            except:
                a='a'
        return articles

    @classmethod
    def getBaseContent(self):
        global BASE_URL
        return Utils.getContent(BASE_URL)


    @classmethod
    @Utils.exceptionHandler
    def getDateFromArticle(self,content):
        for spanned in content.find_all("span"):
            try:
                #print(spanned.string[0:2])
                #checking if is a date
                datecheck = int(spanned.string[0:2])
                return str(spanned.string)
            except:
                a='a'

    @classmethod
    @Utils.exceptionHandler
    def getTitleFromArticle(self, content):
        return str(content.find("h1").string)

    @classmethod
    @Utils.exceptionHandler
    def getBodyFromArticle(self, content):
        return str(content.find("p"))

    @classmethod
    def getArticleAttrs(self,content):
        title = Thairath.getTitleFromArticle(content)
        date = Thairath.getDateFromArticle(content)
        body = Thairath.getBodyFromArticle(content)
        dic = {}
        dic['title'] = title
        dic['date'] = date
        dic['body'] = body
        return dic


    @classmethod
    def getArticle(self):
        url = 'https://www.thairath.co.th'
        #links = getCategoryLinks(getContent(url),url)
        surl = 'https://www.thairath.co.th/news'
        #content = getContent(surl)
        #articles = getArticles(content,url)
        sarticle = 'https://www.thairath.co.th/news/business/2000907' #sample article
        articledic = Thairath.getArticleAttrs(Utils.getContent(sarticle)) #storing its attributes in a dictionary
        return articledic

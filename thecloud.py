#!/usr/bin/env python
from utils import Utils

BASE_URL = 'https://readthecloud.co'

class TheCloud:

    @classmethod
    def getContent(self,url):
        return Utils.getContentHard(url)

    @classmethod
    def getCategoryLinks(self,content):
        global BASE_URL
        links = []
        for i in content.find_all("a"):
            if (i.parent.name == "div"):
                if (i.get('href') != None):
                    if (i.get('href')[0] == '/'):
                        if(len(i.get('href')) > 1):
                            links.append(BASE_URL+i.get('href'))
        return links


    @classmethod
    def getBaseContent(self):
        global BASE_URL
        return Utils.getContentHard(BASE_URL)


    @classmethod
    def getArticles(self,content):
    #still gotta find a way to do the see more bit, results are limited
        articles = []
        smth = content.find(attrs={"class": "content-main"})
        for i in content.find(attrs={"class": "content-main"}).find_all("a"):
            if (i.parent.name == 'h2'):
                articles.append(i.get('href'))
        return articles

    @classmethod
    def getTitleFromArticle(self,content):
        return str(content.find("h1").string)

    @classmethod
    def getBodyFromArticle(self,content):
        body = ''
        for i in content.find(attrs={"class":"single-wrap"}).children:
            if (i.name in ['h1','h2','h3','h4','h5','p'] and i.string != None):
                body += i.string
        return body

    @classmethod
    def getDateFromArticle(self,content):
        return content.find(attrs={"class": "date"}).string.strip()

    @classmethod
    def getArticleAttrs(self,content):
        title = TheCloud.getTitleFromArticle(content)
        #print(title)
        date = TheCloud.getDateFromArticle(content)
        #print(date)
        body = TheCloud.getBodyFromArticle(content)
        #print(body)
        dic = {}
        dic['title'] = title
        dic['date'] = date
        dic['body'] = body
        return dic

    @classmethod
    def getArticle(self):
        url = 'https://readthecloud.co'
        #links = getTheCloudLinks(getContentHard(url),url)
        slink = 'https://readthecloud.co/art-and-culture/'
        #articles = getTheCloudArticles(getContentHard(slink))
        sarticle = 'https://readthecloud.co/democratic-school-of-hadera-israel/'
        articledic = TheCloud.getArticleAttrs(Utils.getContentHard(sarticle))
        return articledic

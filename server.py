#!/usr/bin/env python

from flask import Flask
from flask import request
from flask import jsonify
from flask import Response
from flask_cors import CORS

import time
import sys
import ast
import json

from database import Database


app = Flask(__name__)
CORS(app)





@app.route('/',methods=['GET'])
def home():
    return jsonify(Database.retrieveAllArticles())
    #return "hello"

@app.route('/clear/the-matter', methods=['GET'])
def clear_thematter():
    Database.deleteAllArticlesOfSite('The Matter')
    return ""


@app.route('/clear/the-cloud', methods=['GET'])
def clear_thecloud():
    Database.deleteAllArticlesOfSite('The Cloud')
    return ""


@app.route('/clear/thairath', methods=['GET'])
def clear_thairath():
    Database.deleteAllArticlesOfSite('Thairath')
    return ""

@app.route('/the-matter', methods=['GET'])
def retrieve_thematter():
    return jsonify(Database.retrieveAllArticlesOfSite('The Matter'))

@app.route('/the-cloud', methods=['GET'])
def retrieve_thecloud():
    return jsonify(Database.retrieveAllArticlesOfSite('The Cloud'))

@app.route('/thairath', methods=['GET'])
def retrieve_thairath():
    print(jsonify(Database.retrieveAllArticlesOfSite('Thairath')))
    return jsonify(Database.retrieveAllArticlesOfSite('Thairath'))



if __name__ == "__main__":
    app.run(host='0.0.0.0')

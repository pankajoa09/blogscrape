#!/bin/python
from bs4 import BeautifulSoup
from urllib.request import Request,urlopen
from urllib.parse import unquote
import unicodedata
import re
import os
import json

from database import Database

from thairath import Thairath
from thecloud import TheCloud
from thematter import TheMatter
from utils import Utils

# thairath
# thecloud
# thematter




def storeArticle(article):
    id = Database.insertArticle(article)
    return id

def retrieveAllArticles():
    return Database.retrieveAllArticles()


def extractCategoryFromCategoryLink(categoryLink):
    rev = categoryLink[::-1]
    return rev[:rev.find('/')][::-1]

def extractCategoryFromArticleLink(articleLink):
    return articleLink.split('/')[3:-1]

def storeArticleInDatabase(articledic):
    return Database.insertArticle(articledic)
    #if (Database.articleExists(articledic)):
    #    print('?', end='')
    #else:
    #    Database.insertArticle(articledic)



def fetchAllTheMatterArticles():
    categorylinks = TheMatter.getCategoryLinks(TheMatter.getBaseContent())
    articledics = []
    visitedArticleLinks = list(map(lambda x: x['url'],Database.retrieveAllArticlesOfSite('The Matter')))
    for categorylink in categorylinks:
        print('thematter: catlink',Utils.convertIRIToURL(categorylink))
        category = categorylink.split('/')[4:]
        print(category)
        articlelinks = TheMatter.getArticles(Utils.getContent(categorylink))
        for articlelink in articlelinks:
            #print(articlelink)
            if (articlelink not in visitedArticleLinks):
                articledic = TheMatter.getArticleAttrs(Utils.getContent(articlelink))
                articledic['url'] = articlelink
                articledic['category'] = category
                articledic['site'] = 'The Matter'
                articledics.append(articledic)
                storeArticleInDatabase(articledic)
                visitedArticleLinks.append(articlelink)
    return articledics




def fetchAllTheCloudArticles():
    categorylinks = TheCloud.getCategoryLinks(TheCloud.getBaseContent())
    articledics = []
    visitedArticleLinks = list(map(lambda x: x['url'],Database.retrieveAllArticlesOfSite('The Cloud')))
    for categorylink in categorylinks:
        print('thecloud: catlink',categorylink)
        articlelinks = TheCloud.getArticles(TheCloud.getContent(categorylink))
        for articlelink in articlelinks:
            print(articlelink)
            if (articlelink not in visitedArticleLinks):
                articledic = TheCloud.getArticleAttrs(Utils.getContentHard(articlelink))
                articledic['url'] = articlelink
                articledic['category'] = [categorylink.split('/')[-2]] + [articlelink.split('/')[-2]]
                articledic['site'] = 'The Cloud'
                print(categorylink.split('/')[-2] + articlelink.split('/')[-2])
                articledics.append(articledic)
                storeArticleInDatabase(articledic)
                visitedArticleLinks.append(articlelink)
    return articledics




def fetchAllThairathArticles():
    categorylinks = Thairath.getCategoryLinks(Thairath.getBaseContent())
    articledics = []
    visitedArticleLinks = list(map(lambda x: x['url'],Database.retrieveAllArticlesOfSite('Thairath')))
    for categorylink in categorylinks:
        print('thairath: catlink', categorylink)
        articlelinks = Thairath.getArticles(Thairath.getContent(categorylink))
        for articlelink in articlelinks:
            if (articlelink not in visitedArticleLinks):
                print(articlelink)
                articledic = Thairath.getArticleAttrs(Thairath.getContent(articlelink))
                articledic['url'] = articlelink
                articledic['category'] = extractCategoryFromArticleLink(articlelink)
                articledic['site'] = 'Thairath'
                articledics.append(articledic)
                storeArticleInDatabase(articledic)
                visitedArticleLinks.append(articlelink)
    return articledics






if __name__ == '__main__':
    url = 'https://www.thairath.co.th/news/foreign/2071228'
    #print(extractSubCategoryFromArticleLink(url))
    #Database.deleteAllArticlesOfSite('The Matter')
    fetchAllTheMatterArticles()
    fetchAllTheCloudArticles()
    fetchAllThairathArticles()

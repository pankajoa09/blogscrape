#!/usr/bin/env python

import pymongo
from pymongo import MongoClient

client = MongoClient()
DATABASE = client['blogscrape'].blogscrape


class Database:

    @classmethod
    def createCollection(self,colname):
        client = MongoClient()
        db = client[colname]
        return db

    @classmethod
    def clear(self):
        client = MongoClient()
        client.drop_database('blogscrape')

    @classmethod
    def test(self):
        data = {
            "name": "Pankaj",
            "content": "Some content"
        }
        Database.insertArticle(data)


    @classmethod
    def articleExists(self,article):
        global DATABASE
        print(len(list(DATABASE.find({'url':article['url']}))))
        if (len(list(DATABASE.find({'url':article['url']}))) > 0):
            for i in DATABASE.find({'url':article['url']}):
                a = 'a'
                #print(i)
        return len(list(DATABASE.find({'url':article['url']}))) > 0

    @classmethod
    def insertArticle(self,article):
        global DATABASE
        return DATABASE.insert_one(article).inserted_id

    @classmethod
    def retrieveAllArticles(self):
        global DATABASE
        lst = []
        for i in DATABASE.find():
            i['_id'] = str(i['_id'])
            print(i)
            lst.append(i)
        return lst

    @classmethod
    def retrieveAllArticlesOfSite(self,site):
        global DATABASE
        lst = []
        for i in DATABASE.find({'site':site}):
            i['_id'] = str(i['_id'])
            lst.append(i)
        return lst

    
    @classmethod
    def deleteAllArticlesOfSite(self,site):
        global DATABASE
        return DATABASE.delete_many({'site':site})


    @classmethod
    def deleteArticle(self,article_id, db):
        return ""

    @classmethod
    def replaceArticle(self, article, db):
        return ""


if __name__ == "__main__":
    Database.retrieveAllArticles()

Be sure to have pyhon3, pip3, and MongoDB installed and running.

Install all the requirements via the following:
```
cd blogscrape
pip3 install -r requirements.txt
pip3 install html5lib
```

To run a scan:
```
python3 main.py
```

To run a cronjob that runs at midnight everyday and logs itself in file called `logs`:
```
./startcron
```

To view the scraped blogs via a rudimentary web user interface you need to clone and run https://github.com/pankajoa09/blogscrape-front in a seperate folder and run the following command in this folder:
```
python3 server.py
```



